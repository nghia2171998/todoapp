/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import { View, Text, StyleSheet, TextInput, Button, Image, StatusBar,ImageBackground, ScrollView,TouchableOpacity} from "react-native";
import Icon from './src/icon';
import Task from './src/task';
import Footer from './src/footer';
import Header from './src/header';



App = () => { 
  const [value, setValue] = useState('');
  const [todos, setToDos] = useState([
    // {text: 'nghia', key : '1'}
  ]);
  // const [task , setTask] = useState(0);
  const countTask = todos.length;
  const countComplete = todos.filter(todo=>todo.checked).length;
  const countOpen = todos.filter(todo=>!todo.checked).length ;
  const handleAddToDo = () => {
      if(value.length >0 ){
        setToDos([
          ...todos,
          {
            text: value,
            key : Date.now(),
            checked: false
          }
          
        ])
        setValue('');
      }
  };
  const handleDelete = (id)=> {
      setToDos( todos.filter((todo) =>{
        if(todo.key !== id) return true
      }))
  };

  const handleChecked = (id) => {
      setToDos(todos.map((todo) => {
        if(todo.key === id) todo.checked = ! todo.checked;
        return todo;
      }))
  }
  
  return(
    <View style = {{flex: 1}}>
      <Header style = {styles.img}/>
      <View style={styles.middle}>
        <View style={styles.input_area} >
              <TextInput
                placeholder="Add task..."
                multiline={true}
                style={styles.input}
                onChangeText = {(value)=>{setValue(value)}}
                value = {value}
        
              />
              <TouchableOpacity>
                    <Icon name="pencil" size={30} color="black" style={{ marginLeft: 15 }}
                    onPress={()=>handleAddToDo()}
                    />
              </TouchableOpacity>
      </View>
          

          <ScrollView style={styles.content} >
            {todos.map((task)=>(
              <Task
                  text = {task.text}
                  key = {task.key}
                  checked = {task.checked}
                  setChecked = {()=>handleChecked(task.key)}
                  delete = {() => handleDelete(task.key) }
              />
            ))}
           
          </ScrollView>
          {countTask == 0 && <Text style={styles.notTask}>You're all caught up!</Text>}
          
      </View>
     <Footer  style={styles.footer}
          task= {countTask}
          complete = {countComplete}
          open = {countOpen}
     />
    </View>
  )
}

const styles = StyleSheet.create({
    img:{
      flex: 1,
      height : 100
    },
    input_area:{
      flexDirection: 'row',
      paddingRight: 10,
      paddingBottom: 5
    },
    input:{
      width: 300,
      flex: 2,
      color: 'black',
     
    },
    content:{
      flex: 4,
    },
    footer: {
      flex: 0.25,
    },
    pencil:{
      paddingTop: 7
    },
    item_uncheckedeck: {
      flex: 1
    },
    middle:{
      flex: 4,
      padding: 8,
    },
    item: {
      flexDirection: "row",
      padding: 10
    },
    item_text:{
      flex: 3,
      paddingLeft: 5
    },
    notTask :{
    paddingVertical: 150,
     paddingHorizontal: 80,
     fontSize: 15
      
    }

})
console.log(new Date().toLocaleString('en-us', {  weekday: 'long' }));
export default App