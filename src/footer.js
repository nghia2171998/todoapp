import React, {useState} from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

export default function footer(props) {
    return (
    <View style = {styles.footer}>
        <Text> {props.task} tasks</Text>
        <Text> {props.complete} complete</Text>
        <Text>  {props.open} open</Text>
    </View>
  
    )
}

const styles = StyleSheet.create({
    footer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        padding : 10
    }
})
