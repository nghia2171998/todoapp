import React, {useState} from 'react'
import { View, TouchableOpacity, Text, StyleSheet,Image } from 'react-native'

const weekday = ['Sunday',
               'Monday',
               'Tuesday',
               'Wednesday',
               'Thursday',
               'Friday',
               'Saturday'][new Date().getDay()];
const Month = ["January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"][new Date().getMonth()];
const hour = new Date().getHours();
export default function header() {
    return (
    <View>
        {hour >= 6 && hour < 16  &&  <Image source={require("../image/sang.jpg")}style = {styles.img}/> }
        {hour >= 16 && hour < 20  &&  <Image source={require("../image/chieu.jpg")}style = {styles.img}/> }
        {hour >= 20 || hour <= 5  &&  <Image source={require("../image/toi.jpg")}style = {styles.img}/> }
        <Text style = {styles.weekday}>{weekday}</Text>
        <Text style = {styles.month}>{Month},</Text>
        <Text style = {styles.date}>{new Date().getDate()}</Text>
    </View>
    )
}
const styles = StyleSheet.create({
    img : {
        height: 150,
    },
    weekday: {
        position : "absolute",
        color: "white",
        paddingHorizontal: 30,
        fontSize: 25,
        top : 20
    },
    month:{
        position : "absolute",
        color: "white",
        paddingHorizontal: 30,
        top: 60,
        fontSize: 15
    },
    date:{
        position : "absolute",
        color: "white",
        left: 77,
        top: 60,
        fontSize: 15
    }
})