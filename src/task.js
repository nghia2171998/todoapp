import React, {useState} from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Icon from './icon';
const Task = (props) => (
    <View style={styles.taskWrapper}>
        <Icon
            name= { props.checked ? "checkbox-checked" :  "checkbox-unchecked" }
            size={25}
            color= { props.checked ? "#DB2777" :  "black" }
            style={{ marginLeft: 15 }}
            onPress = {props.setChecked}
        />
        <View>
        {props.checked ?  <Text style={styles.task_checked}>{props.text}</Text> :  <Text style={styles.task}>{props.text}</Text>  }
       
        </View>
        <Icon
            name="bin"
            size={25}
            color="black"
            style={{ marginLeft: 'auto' }}
            onPress={props.delete}
        />
    </View>
)
export default Task
const styles = StyleSheet.create({
    taskWrapper: {
        marginTop: '5%',
        flexDirection: 'row',
        borderColor: '#e5e7eb',
        borderBottomWidth: 1,
        width: '100%',
        alignItems: 'center',
        minHeight: 40,
    },
    task: {
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop: 6,
        borderColor: '#F0F0F0',
        borderBottomWidth: 1,
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    task_checked:{
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop: 6,
        borderColor: '#F0F0F0',
        borderBottomWidth: 1,
        fontSize: 17,
        fontWeight: 'bold',
        color: "#DB2777",
        textDecorationLine: 'line-through',
    },
  
})